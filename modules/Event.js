/**
 * The class for work with Events.
 * May add and emit events
 * @constructor
 */
var Event = function() {
    this.events = {};
};

/** This is a pool of all events */
Event.prototype.events = null;

/**
 * Set a new event for further usage
 * @param {string} event - Event name
 * @param {Function} callback - Callback function
 */
Event.prototype.on = function(event, callback) {
    this.events[event] = callback;
};

/**
 * Call event if it exists
 * @param {string} event - Event name
 * @param {object} data - Special object that contains the specified parameters
 */
Event.prototype.emit = function(event, data) {
    if (this.events[event]) this.events[event](data);
};

/** Export variables */
module.exports.class = Event;
module.exports.instance = function() { return new Event() };