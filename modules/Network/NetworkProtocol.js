var NetworkProtocol = function() {};

NetworkProtocol.prototype.generateAnswerOk = function(PACKAGE_ID, PART_ID) {
    var buffer = new Buffer(1 + 4 + 4);
    buffer[0] = 4;
    buffer.writeUInt32BE(PACKAGE_ID, 1);
    buffer.writeUInt32BE(PART_ID, 1 + 4);
    return buffer;
};

NetworkProtocol.prototype.generatePingMessage = function() {
    var buffer = new Buffer(1);
    buffer[0] = 5;
    return buffer;
};

/** Export variables */
module.exports.class = NetworkProtocol;
module.exports.instance = function() { return new NetworkProtocol() };