/** Declared classes for fast work with sockets */
var Server = require("./Server").class;
var Client = require("./Client").class;

/**
 * The class for work with TCP sockets.
 * May create TCP server and TCP client
 * @constructor
 */
var TCP = function() {};

/**
 * Create a new TCP server and return link
 * @param {string} host - IP:Port.
 * @return {Server}
 */
TCP.prototype.createServer = function(host) {
    return new Server(host);
};

/**
 * Create a new TCP client and return link
 * @param {string} host - IP:Port.
 * @return {Client}
 */
TCP.prototype.createClient = function(host) {
    return new Client(host);
};

/** Export variables */
module.exports.class = TCP;
module.exports.instance = function() { return new TCP() };