/**
 * Declare necessary classes
 */
var Util = require('util');
var Event = require('../../Event').class;
var Net = require("net");

/**
 * Represents a TCP client.
 * @constructor
 * @param {string} host - IP:Port
 */
var Client = function(host) {
    this.events = {};
    this.serverIP = host.split(":")[0];
    this.serverPort = host.split(":")[1] * 1;
};

/** Extends our Client class for Events support */
Util.inherits(Client, Event);

/** @member {string} - Represent client IP address */
Client.prototype.clientIP = null;

/** @member {string} - Represent client Port */
Client.prototype.clientPort = null;

/** @member {string} - Represent remote server IP address */
Client.prototype.serverIP = null;

/** @member {string} - Represent remote server Port */
Client.prototype.serverPort = null;

/** @member {Net.Socket} - Represent Socket link */
Client.prototype.clientInstance = null;

/**
 * Connect to remote server and start listening if success
 */
Client.prototype.start = function() {
    /** Link of Client class */
    var self = this;

    /** Create and get instance of new Socket */
    this.clientInstance = new Net.Socket();

    /** The handler for success connection */
    this.clientInstance.on("connect", function() {
        /** Get our local parameters */
        self.clientIP = self.clientInstance.localAddress;
        self.clientPort = self.clientInstance.localPort;

        /** Emit success connected event */
        self.emit("connected", { ip: self.clientIP, port: self.clientPort });
    });

    /** The data handler, emit when the server send data to us */
    this.clientInstance.on("data", function(data) {
        self.emit("data", { message: data });
    });

    /**
     * Connect to remote server
     * @param {number} - Server port
     * @param {string} - Server ip
     */
    this.clientInstance.connect(this.serverPort, this.serverIP);
};

/**
 * Send message to remote server
 * @param {string} message - Just message to translate to remote server
 */
Client.prototype.send = function(message) {
    try {
        this.clientInstance.write(message);
    }
    catch (e) {
        console.log(e);
    }
};

/** Export variables */
module.exports.class = Client;
module.exports.instance = function() { return new Client() };