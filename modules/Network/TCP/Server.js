/**
 * Declare necessary classes
 */
var Util = require('util');
var Event = require('../../Event').class;
var MessageProtocol = require("../MessageProtocol").instance();
var Net = require("net");

/**
 * Represents a TCP server.
 * @constructor
 * @param {string} host - IP:Port
 */
var Server = function(host) {
    this.events = {};
    this.serverIP = host.split(":")[0];
    this.serverPort = host.split(":")[1] * 1;
};

/** Extends our Server class for Events support */
Util.inherits(Server, Event);

/** @member {string} - Represent server IP address */
Server.prototype.serverIP = null;

/** @member {number} - Represent server Port */
Server.prototype.serverPort = null;

/** @member {Net} - Represent TCP server link */
Server.prototype.serverInstance = null;

/** @member {Array} - Contains all active clients */
Server.prototype.clientList = [];

/**
 * Start TCP server and add listeners
 */
Server.prototype.start = function() {
    /** Link of Server class */
    var self = this;

    /** Create and get instance of new TCP server */
    this.serverInstance = Net.createServer(null);

    /**
     * Starts accepting new clients
     */
    this.serverInstance.on("listening", function() {
        self.emit("start", {});
    });

    /**
     * The client has been connected to server
     * @param {Net} socket - Client socket
     */
    this.serverInstance.on("connection", function(socket) {
        /** Push new client to clientList */
        self.clientList.push(socket);

        /** Add remove handler to track when the client has been disconnected
         * system will remove it from clientList */
        socket.on("close", function() {
            self.clientList.splice(self.clientList.indexOf(socket), 1);
        });

        /** Emit connect event */
        self.emit("connect", { socket: socket });
    });

    /**
     * Server error handler
     */
    this.serverInstance.on("error", function() {
        self.emit("error", {});
    });

    /**
     * Starts listen our server
     * @param {number} - Server port
     * @param {string} - Server ip
     */
    this.serverInstance.listen(this.serverPort, this.serverIP);

    /** It's handler for exceptions to avoid crash server */
    process.on('uncaughtException', function (err) { console.log(err); });
};

/**
 * Send messages to all clients except sender
 * @param {Net.Socket} me - Sender socket
 * @param {*} message - Message to send
 * @param {string} protocol - Data handler type
 * @param {Function} handler - Special function for processing message
 */
Server.prototype.broadcast = function(me, message, protocol, handler) {
    /** Handle each active client */
    for (var client in this.clientList) {
        /** Just convenience short link */
        var currentClient = this.clientList[client];

        /** Skip sender */
        if (currentClient == me) continue;

        /** Check if handler was set */
        if (handler)
            this.send(currentClient, handler(currentClient, message), protocol);
        else
            this.send(currentClient, message, protocol);
    }
};

/**
 * Send message for concrete client
 * @param {Net.Socket} user - Client socket who we send message
 * @param {string} message - Message must be string
 * @param {string} protocol - Data handler type
 * @param {Function} handler - Special function for processing message
 */
Server.prototype.send = function(user, message, protocol, handler) {
    try {
        if (handler)
            user.write(MessageProtocol.translate(protocol, handler(user, message)));
        else
            user.write(MessageProtocol.translate(protocol, message));
    }
    catch (e) {
        //error
    }
};

/** Export variables */
module.exports.class = Server;
module.exports.instance = function(host) { return new Server(host) };