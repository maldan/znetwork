/**
 * The class for processing message.
 * Take a message, processing it and just return a result
 * @constructor
 */
var MessageProtocol = function() {};

/**
 * Represent a JSON stringify. Just take and return JSON string
 * @param {*} message - It could be anything
 */
MessageProtocol.prototype.JSON = function(message) {
    return JSON.stringify(message);
};

MessageProtocol.prototype.Binary = function(message) {
    return new Buffer(message);
};

/**
 * Just safe wrapper to avoid exceptions
 * @param {string} name - Function name
 * @param {*} message - It could be anything
 */
MessageProtocol.prototype.translate = function(name, message) {
    if (this[name])
        return this[name](message);
    else
        return message;
};

/** Export variables */
module.exports.class = MessageProtocol;
module.exports.instance = function() { return new MessageProtocol() };