/**
 * Declare necessary classes
 */
var Util = require('util');
var Event = require('../../Event').class;
var DGram = require('dgram');
var Queue = require('./Queue').class;
var DataPackage = require('./DataPackage').class;
var MessageProtocol = require('../MessageProtocol').instance();
var NetworkProtocol = require('../NetworkProtocol').instance();

/**
 * The class for work with UDP sockets.
 * May send datagram and listen UDP ports
 * @constructor
 */
var UDP = function() {
    this.events = {};
};

/** Extends our Server class for Events support */
Util.inherits(UDP, Event);

UDP.prototype.serverInstance = null;
UDP.prototype.networkPool = [];
UDP.prototype.queuePool = [];

UDP.prototype.createServer = function(port) {
    var self = this;

    this.serverInstance = DGram.createSocket('udp4');
    this.serverInstance.classRoot = this;

    this.serverInstance.on('listening', function () {
        self.emit("start", { port: port });
    });

    this.serverInstance.on('message', this.answerHandler);
    //function (message, client) {
        /*if (message) {
            if (message[0] == 5) {
                console.log("Ping from " + client.address + ":" + client.port);
            }
        }*/
        /*self.emit("data", {
            message: message,
            client: client
        });*/
    //});

    this.serverInstance.on('close', function () {
        self.emit("close", {});
    });

    this.serverInstance.on('error', function () {
        self.emit("error", {});
    });

    this.serverInstance.bind(port);

    return this.serverInstance;
};

UDP.prototype.answerHandler = function(message, client) {
    var self = this.classRoot;
    var packageID, partID, dataLength, bufferData;

    switch (message[0]) {
        case 1:
            packageID = message.readUInt32BE(1);
            partID = message.readUInt32BE(1 + 4);
            dataLength = message.readUInt32BE(1 + 4 + 4);
            bufferData = new Buffer(dataLength);

            message.copy(bufferData, 0, 1 + 4 + 4 + 4, message.length);

            self.sendOKPackage({
                ip: client.address,
                port: client.port
            }, packageID, partID);

            self.emit("data", {
                message: bufferData,
                client: client
            });

            break;
        case 4:
            packageID = message.readUInt32BE(1);
            partID = message.readUInt32BE(1 + 4);

            for (var i = 0; i < self.queuePool.length; i++) {
                self.queuePool[i].packageReceived({
                    packageID: packageID,
                    partID: partID,
                    client: client
                });
            }
            break;
        case 5:
            //console.log("Ping from " + client.address + ":" + client.port);
            break;
        default:
            break;
    }
};

UDP.prototype.broadcast = function(me, buffer) {
    var self = this;

    for (var i = 0; i < self.networkPool.length; i++) {
        this.serverInstance.send(
            buffer, 0,
            buffer.length,
            self.networkPool[i][1], self.networkPool[i][0]
        );
    }
};

UDP.prototype.sendTo = function(to, buffer) {
    var self = this;

    this.serverInstance.send(
        buffer, 0,
        buffer.length,
        to.port, to.ip
    );
};

/**
 *
 * @param to
 * @param type - Ping, Message, Binary Block, File
 * @param message - Data block
 */
UDP.prototype.sendDataPackage = function(to, buffer) {
    var queue = new Queue(this.serverInstance);
    var packageSize = 49152;
    var parts = Math.ceil(buffer.length / packageSize);
    var packageID = Math.ceil(Math.random() * 16777216);
    var currentPackage = null;
    var lastPackageSize = (Math.ceil(buffer.length / packageSize) * packageSize) - buffer.length;

    this.queuePool.push(queue);

    if (parts == 1) {
        currentPackage = new DataPackage();
        currentPackage.create(to, packageID, 0, buffer);
        queue.add(currentPackage);
    } else {
        for (var i = 0; i < parts; i++) {
            var buffSize = packageSize;
            if (i == parts - 1) buffSize = lastPackageSize;
            var newBuffer = new Buffer(buffSize);
            buffer.copy(newBuffer, 0, i * packageSize, i * packageSize + buffSize);

            currentPackage = new DataPackage();
            currentPackage.create(to, packageID, 0, newBuffer);
            queue.add(currentPackage);
        }
    }

    var self = this;

    queue.complete = function(currentQueue) {
        self.queuePool.splice(self.queuePool.indexOf(currentQueue), 1);
    };

    queue.start();
};

UDP.prototype.broadcastDataPackages = function(buffer) {
    var self = this;

    for (var i = 0; i < self.networkPool.length; i++) {
        this.sendDataPackage({
            ip: self.networkPool[i][0],
            port: self.networkPool[i][1]
        }, buffer);
    }
};

UDP.prototype.sendOKPackage = function(to, packageID, partID) {
    var message = NetworkProtocol.generateAnswerOk(packageID, partID);
    this.sendTo(to, message);
};

UDP.prototype.makeForwarding = function() {
    var message = NetworkProtocol.generatePingMessage();
    this.broadcast(null, message);
};

UDP.prototype.keepAlive = function() {
    var self = this;
    setInterval(function() {
        self.makeForwarding();
    }, 3000);
};

/** Export variables */
module.exports.class = UDP;
module.exports.instance = function() { return new UDP() };