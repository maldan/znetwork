var Queue = function(udpServer) {
    this.udpServer = udpServer;
    this.elements = [];
};

Queue.prototype.udpServer = null;
Queue.prototype.elements = null;
Queue.prototype.currentPackage = null;
Queue.prototype.complete = null;
Queue.prototype.timeOutID = null;

Queue.prototype.add = function(pack) {
    this.elements.push(pack);
};

Queue.prototype.start = function() {
    this.run();
};

Queue.prototype.packageReceived = function(data) {
    var packageID = data.packageID;
    var partID = data.partID;
    var client = data.client;

    if (client.address == this.currentPackage.receiverIP
        && client.port == this.currentPackage.receiverPort
        && packageID == this.currentPackage.ID)
    {
        clearTimeout(this.timeOutID);

        /** It was last package */
        if (this.elements.length == 0) {
            this.safeDestroy();
            return;
        }

        this.run();
    }
};

Queue.prototype.run = function(repeat) {
    if (!repeat && this.elements.length) this.currentPackage = this.elements.shift();

    this.udpServer.send(
        this.currentPackage.buffer, 0,
        this.currentPackage.buffer.length,
        this.currentPackage.receiverPort, this.currentPackage.receiverIP
    );

    var self = this;

    clearTimeout(this.timeOutID);
    this.timeOutID = setTimeout(function() {
        self.run(true);
    }, 1000);
};

Queue.prototype.safeDestroy = function() {
    if (this.complete) this.complete(this);
};


/** Export variables */
module.exports.class = Queue;
module.exports.instance = function() { return new Queue() };