var DataPackage = function() {};

DataPackage.prototype.ID = null;
DataPackage.prototype.partID = null;
DataPackage.prototype.message = null;
DataPackage.prototype.buffer = null;
DataPackage.prototype.receiverIP = null;
DataPackage.prototype.receiverPort = null;
DataPackage.prototype.senderIP = null;
DataPackage.prototype.senderPort = null;

DataPackage.prototype.create = function(to, id, partId, bytes) {
    this.receiverIP = to.ip;
    this.receiverPort = to.port;
    this.ID = id;
    this.partID = partId;
    this.message = bytes;

    this.buffer = new Buffer(1 + 4 + 4 + 4 + bytes.length);

    this.buffer[0] = 1; //data
    this.buffer.writeUInt32BE(id, 1);
    this.buffer.writeUInt32BE(partId, 1 + 4);
    this.buffer.writeUInt32BE(bytes.length, 1 + 4 + 4);

    bytes.copy(this.buffer, 1 + 4 + 4 + 4, 0, bytes.length);
};

/** Export variables */
module.exports.class = DataPackage;
module.exports.instance = function() { return new DataPackage() };