var TCP = require("./modules/Network/TCP/TCP").instance();
var IP_Table = {};
var Server1 = TCP.createServer("109.234.155.117:50777");

Server1.start();

Server1.on("start", function() {
    console.log("Server starts");
});

function messageHandler(client, message) {
    var NEW_IP_TABLE = {};

    for (var ipaddr in message)
    {
        if (client.remoteAddress == ipaddr) {
            NEW_IP_TABLE[ipaddr] = [].concat(message[ipaddr]);

            var PORT_ID = NEW_IP_TABLE[ipaddr].indexOf(client.specialPort);
            if (PORT_ID != -1) NEW_IP_TABLE[ipaddr].splice(PORT_ID, 1);
            if (!NEW_IP_TABLE[ipaddr].length) delete NEW_IP_TABLE[ipaddr];

        } else NEW_IP_TABLE[ipaddr] = [].concat(message[ipaddr]);
    }

    return NEW_IP_TABLE;
}

Server1.on("connect", function(data) {
    var user = data.socket;
    var ip = user.remoteAddress;
    var port = user.remotePort;

    user.on("data", function(message) {
        var message = message.toString().split(":");

        if (message[0] == "cn") {
            if (!message[1]) return;
            if (!IP_Table[ip]) IP_Table[ip] = [];
            IP_Table[ip].push(message[1] * 1);
            user.specialPort = message[1] * 1;

            /*IP_Table[ip].push(port);
            user.specialPort = port;*/
        }

        Server1.broadcast(null, IP_Table, "JSON", messageHandler);
    });

    user.on("close", function() {
        if (IP_Table[ip]) {
            var IPID = IP_Table[ip].indexOf(user.specialPort);
            if (IPID != -1) IP_Table[ip].splice(IPID, 1);
            if (!IP_Table[ip].length) delete IP_Table[ip];
        }

        Server1.broadcast(user, IP_Table, "JSON", messageHandler);
    });
});