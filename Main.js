var TCP = require("./modules/Network/TCP/TCP").instance();
var UDP_Server = require("./modules/Network/UDP/UDP").instance();

var Client1 = TCP.createClient("109.234.155.117:50777");
var LocalPort = 50666;
var SendedReceived = [0, 0];
var DATAS = {};
var ReceiveSpeed = 0;

Client1.start();
UDP_Server.createServer(LocalPort);

Client1.on("connected", function(data) {
    Client1.send("cn:" + LocalPort);
});

Client1.on("data", function(data) {
    var report = JSON.parse(data.message);

    UDP_Server.networkPool = [];

    for (var client in report) {
        for (var i = 0; i < report[client].length; i++) {
            UDP_Server.networkPool.push([client, report[client][i]]);
        }
    }

    UDP_Server.makeForwarding();

    //console.log(UDP_Server.networkPool);
});

UDP_Server.on("start", function(data) {
    console.log("Local server start on port: " + data.port);

    UDP_Server.keepAlive();



    setTimeout(function() {
        var fs = require('fs');
        var content = fs.readFileSync('VIDEO0069.mp4');
        //UDP_Server.broadcastDataPackages(content);

        /*if (UDP_Server.networkPool.length > 0) {
            for (var i = 0; i < 2000; i++) {
                var message = new Buffer("Test");
                UDP_Server.broadcastDataPackages(message);
                SendedReceived[0]++;
            }
        }*/
    }, 3000);

    setInterval(function() {
        process.stdout.write('\033c');
        //console.log("Ratio: " + Object.keys(DATAS).length);
        /*console.log("Ratio: " + SendedReceived.toString());
        console.log("Clients: " + UDP_Server.networkPool.length);
        console.log("Queues: " + UDP_Server.queuePool.length);*/
        console.log("Receive speed: " + (ReceiveSpeed / 1024).toFixed(2) + " kb/s");
        ReceiveSpeed = 0;
    }, 1000);
});

UDP_Server.on("data", function(data) {
    var message = data.message;
    var client = data.client;

    //DATAS[message.toString() * 1] = true;
    //if (message.toString() == "Test") SendedReceived[1]++;

    /*var fs = require('fs');
    fs.writeFile("out.png", message, "a+");*/

    ReceiveSpeed += message.length;

    var fs = require('fs');
    fs.appendFile('out.mp4', message, encoding='utf8', function (err) {
        if (err) throw err;
    });

    /*var fs = require('fs');
    var log = fs.createWriteStream("out.png", {'flags': 'a'});
    log.end(message);*/
});
